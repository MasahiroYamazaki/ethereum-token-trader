require 'ethereum'

class EthSendService
  def initialize(from_user, to_user, eth_value)
    @from_user = from_user
    @to_user = to_user
    @client = Ethereum::IpcClient.new(Token::ETHEREUM_PATH)
    @eth_value = eth_value
    @hex_value = "0x#{eth_value.to_s(16)}"
  end

  def perform
    @client.personal_unlock_account(@from_user.address, @from_user.ether_password)
    eth_address = send_transaction(@from_user.address, @to_user.address, @hex_value)
    detail = transaction_detail(eth_address)
    detail # detail['hash']がトランザクションのアドレス
  end

  private

  def send_transaction(from_address, to_address, hex_value)
    hash = {}
    hash['from']  = from_address
    hash['to']    = to_address
    hash['value'] = hex_value
    trs_result = @client.eth_send_transaction(hash)
    trs_hash = trs_result['result']
    trs_hash
  end

  def transaction_detail(eth_address)
    transaction = @client.eth_get_transaction_by_hash(eth_address)
    transaction['result']
  end
end
