module TradesHelper
  def not_current_users
    User.all.reject { |u| u == current_user }
  end
end
