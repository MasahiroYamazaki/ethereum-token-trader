# == Schema Information
#
# Table name: trade_details
#
#  id                 :integer          not null, primary key
#  user_id            :integer
#  trade_id           :integer
#  ether              :decimal(, )
#  token_amount       :decimal(, )
#  eth_send_address   :integer
#  token_send_address :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class TradeDetail < ApplicationRecord
  belongs_to :user
  belongs_to :trade, optional: true

  validates :token_amount, numericality: { only_integer: true, greater_than: 0 }
end
