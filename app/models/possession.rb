# == Schema Information
#
# Table name: possessions
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  token_id   :integer
#  balance    :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Possession < ApplicationRecord
  belongs_to :token
  belongs_to :user
end
