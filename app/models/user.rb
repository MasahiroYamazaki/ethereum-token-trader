# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  address                :string
#  ether_password         :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

require 'ethereum'

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :trades, dependent: :nullify
  has_many :possessions, dependent: :nullify

  ETHEREUM_PATH = '/Users/rebit_n005/testnet/geth.ipc'.freeze

  # ユーザーの現在のETH所持量を確認する
  def eth_balance
    @client = Ethereum::IpcClient.new(ETHEREUM_PATH)
    @client.personal_unlock_account(address, ether_password)
    result_hash = @client.eth_get_balance(address)
    result_hash['result'].hex / 10**18
  end
end
